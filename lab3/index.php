<?php
/**
 * @author: Hyei-In Suh
 * @comments: 
 * @purpose: to get the validated user input to store in database.
 */

//dentifying the hostname, database name, user account and password for connection
define("DBHOST", "localhost");
define("DBDB", "demo");
define("DBUSER", "lamp1user");
define("DBPW", "!Lamp12!");

//open the database connection
function connectDB(){
    $dsn = "mysql:host=".DBHOST.";dbname=" .DBDB.";charset=utf8";
    try{
        $db_conn = new PDO($dsn, DBUSER, DBPW);
        return $db_conn;
    }
    catch(PDOException $e){
        echo "<p>Error opening database<br />\n".$e->getMessage()."</p>\n";
        exit(1);
    }
}

//PDO insert function
function insertContacts($fname, $lname, $email, $emailPersonal, $phone, $phonePersonal){
    //open the data connection
    $db_conn = connectDB();
    
    //sql query while protecting sql injection attack
    $stmt = $db_conn -> prepare("INSERT INTO lab3(first_name,last_name, email, email_personal, phone, phone_personal) VALUES(:first_name,:last_name, :email, :email_personal, :phone, :phone_personal)");
    
    //error detecting
    if(!$stmt){
        echo "Error ".$db_conn->errorCode()."\nMessage" .implode($db_conn->errorInfo())."\n";
        exit(1);
    }

    //set data in to parameter
    $data = array(":first_name" => $fname,":last_name"=> $lname, ":email"=> $email, ":email_personal"=> $emailPersonal, ":phone"=> $phone, ":phone_personal"=> $phonePersonal);
    
    //execute data while protecting sql injection attack
    $status = $stmt->execute($data);

    if(!$status){
        echo "Error " .$stmt->errorCode()."\nMessage ".implode($stmt->errorInfo())."\n";
        exit(1);
    }

    //close the data connection
    $db_conn = null;
}

//declare the global variable
$fname = $lname = $email = $emailPersonal = $phone = $phonePersonal = "";

//function to validate the form
function validateForm(){
    //declare the array to handle the errors 
    $err_msgs= array();

    //global variable 
    global $fname;
    global $lname;
    global $email;
    global $phone;
    global $emailPersonal;
    global $phonePersonal;
    

    //validate the fields(frstname)
    if(!isset($_POST['first_name'])){
        $err_msgs[] = "Your first name shouldnt be empty spaces";
    }
    else{
        if(strlen($_POST['first_name']) > 50){
            $err_msgs[] = "Your first name is Too long";
        }
        $trimFname = trim($_POST['first_name']);

        if(strlen($trimFname)==0){
            $err_msgs[] = "Your first name shouldnt be empty spaces";

        }

        if(count($err_msgs)==0){
            $fname = $_POST['first_name'];
        }  

    }

    //validate the fields(lastname)
    if(!isset($_POST['last_name'])){
        $err_msgs[] = "Your last name shouldnt be empty spaces";
    }
    else{
        if(strlen($_POST['last_name']) > 50){
            $err_msgs[] = "Your last name is Too long";
        }

        $trimLname = trim($_POST['last_name']);
        if(strlen($trimLname)==0){
            $err_msgs[] = "Your last name shouldnt be empty spaces";
        }

        if(count($err_msgs)==0){
           $lname = $_POST['last_name'];
        }  
    }

    //validate the fields(email)
    if(!isset($_POST['email'])){
        $err_msgs[] = "Your email shouldnt be empty spaces";
    }
    else{
        if(strlen($_POST['email']) > 128){
            $err_msgs[] = "Your email is Too long";
        }

        $trimemail = trim($_POST['email']);
        if(strlen($trimemail)==0){
            $err_msgs[] = "Your email shouldnt be empty spaces";
        }

        if(count($err_msgs)==0){
           $email = $_POST['email'];
        }  
    }

    //validate the fields(phone)
    if(!isset($_POST['phone'])){
        $err_msgs[] = "Your phone shouldnt be empty spaces";
    }
    else{
        if(strlen($_POST['phone']) > 20){
            $err_msgs[] = "Your phone is Too long";
        }

        $trimphone = trim($_POST['phone']);
        if(strlen($trimphone)==0){
            $err_msgs[] = "Your phone shouldnt be empty spaces";
        }

        if(count($err_msgs)==0){
           $phone = $_POST['phone'];
        }  
    }

    //return error
    return $err_msgs;
}


//actions when the form submitted on POST method
if($_SERVER['REQUEST_METHOD'] == 'POST'){
    global $validations;

    $validations = validateForm();

    foreach($validations as $validation){
        echo "<p>".$validation."</p>";
    }

    //convert the value of the checkbox to 1,0
    if(count($validations) == 0){
        if(isset($_POST['emailPersonal']) && $_POST['emailPersonal'] == "on"){
            $emailPersonal = 1;
        }
        else{
            $emailPersonal = 0;
        }
        if(isset($_POST['phonePersonal']) && $_POST['phonePersonal']== "on"){
            $phonePersonal = 1;
        }
        else{
            $phonePersonal = 0;
        }
        //insert the validated data in to database
        insertContacts($fname, $lname, $email, $emailPersonal, $phone, $phonePersonal);
    }     

    //$fname = $lname = $email = $emailPersonal = $phone = $phonePersonal = "";
 
}

//functions to set the value of the text box when $_POST is set
function getPost($key){
        return isset($_POST[$key]) ? $_POST[$key] : '';    
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Contacts</title>
</head>
<body>
    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
        <div>
            <label for="first_name">First Name</label>
            <input type="text" name="first_name" id="first_name" value="<?php echo getPost('first_name')?>">
        </div>

        <br />

        <div>
            <label for="last_name">Last Name</label>
            <input type="text" name="last_name" id="last_name" value="<?php echo getPost('last_name')?>">
        </div>
        
        <br />

        <div>
            <label for="email">Email</label>
            <input type="text" name="email" id="email" value="<?php echo getPost('email')?>">
        </div>
        
        <br />

        <div>
            <label for="personal">This is personal email</label>
            <input type="checkbox" name="emailPersonal" <?php echo (empty($_POST['emailPersonal'])) ? '':'checked' ?>> 
        </div>

        <br />

        <div>
            <label for="phone">Phone</label>
            <input type="text" name="phone" id="phone" value="<?php echo getPost('phone')?>">
        </div> 

        <br />

        <div>
            <label for="personal">This is personal phone number</label>
            <input type="checkbox" name="phonePersonal" <?php echo (empty($_POST['phonePersonal'])) ? '':'checked' ?>/>
        </div>

        <br />

        <input type="submit" value="Submit">
    </form>
</body>
</html>