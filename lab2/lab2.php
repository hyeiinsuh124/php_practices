<?php
	declare(strict_types=1);
?>
<!--
Purpose: To include the functions that does mathematics for random amount of
		 spheres and surface, and volumes
Author: Hyei-in Suh
-->
<?php
//connect database using lab2db.php
	include 'lab2db.php';

	//function to generate certain amount of random size of spheres
	function randomSpheres(int $amount): array {
		$spheres = array();

		for($i = 0; $i < $amount; $i++){
			
			$radii = rand(1,20);
			array_push($spheres, $radii);
		}

		return $spheres;
	}

	//calculate the surface
	function spheresSurface(int $radii): float {

		$surface = 4 * pi() * pow($radii, 2);

		return $surface;
	}

	//calculate the volume
	function spheresVolume(int $radii): float {
		$vol = 4/3 * pi() * pow($radii, 3);

		return $vol;
	}

	$generateSpheres = randomSpheres(2);
	// print_r($generateSpheres);

	//connect the database to insert the datas
	foreach ($generateSpheres as $radius) {
		$generatedSurface = spheresSurface($radius);
		$generatedVolume = spheresVolume($radius);

		insertSpheres($radius, $generatedVolume, $generatedSurface);
	}
	//print_r($GLOBALS['records']);
?>
