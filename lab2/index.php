<!--
Purpose: To display the all database to html with table format using database connection defined in lab2db.php
Author: Hyei-in Suh
-->
<?php 
   	include 'lab2db.php';
?>
<html>
<head>
<title>Lab2</title>
</head>
<body>
    <table>
    <tr>
        <th>Radius</th>
        <th>Volume</th>
        <th>Surface</th>
    </tr>
    
    <?php 
    foreach (selectSpheres() as $rows) 
    {
       echo "<tr>";
       foreach ($rows as $column) {
           echo
            "<td>" . $column . "</td>";
       }
       echo "</tr>";
    }
    ?> 
    </table>
</body>
</html>
