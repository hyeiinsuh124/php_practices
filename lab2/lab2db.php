<!--
Purpose: To open  a PDO connection to the lab2 database using the lamp1user account.
		and to perform the inserting of database from lab2.php and display the data to index.php
Author: Hyei-in Suh
-->
<?php
	define("DBHOST", "localhost");
	define("DBDB",   "lab2");
	define("DBUSER", "lamp1user");
	define("DBPW", "!Lamp12!");
	
	//Connect the databasa
	function connectDB(){
		$dsn = "mysql:host=".DBHOST.";dbname=".DBDB.";charset=utf8";
		try{
			$db_conn = new PDO($dsn, DBUSER, DBPW);
			return $db_conn;
		} catch (PDOException $e){
			echo "<p>Error opening database <br/>\n".$e->getMessage()."</p>\n";
			exit(1);
		}
	}

	//open the database to inset the data	
	function insertSpheres($radius, $volume, $surface_area) {
		$dbc = connectDB();

		$stmt = $dbc->prepare('INSERT INTO spheres (radius, volume, surface_area) values(:radius, :volume, :surface_area)');
	
		if (!$stmt) {
				echo "<p>Error ".$dbc->errorCode()."\nMessage ".implode($dbc->errorInfo())."</p>\n";
				exit(1);
		}
		$data = array(":radius" => $radius, ":volume" => $volume, ":surface_area" => $surface_area);
		$status = $stmt->execute($data);
	
		if(!$status) {
			echo "<p>Error ".$stmt->errorCode()."\nMessage ".implode($stmt->errorInfo())."</p>\n";
			exit(1);
		}

		//close the data
		$dbc = NULL;
	}

	//open the database to select the database to read the datas
	function selectSpheres() {
        $dbc = connectDB();
        $result = array();

        $stmt = $dbc->prepare("SELECT radius, volume, surface_area FROM spheres;");
        if (!$stmt) {
            echo "<p>Error ".$dbc->errorCode()."\nMessage ".implode($dbc->errorInfo())."</p>\n";
            exit(1);
        }

        $status = $stmt->execute();

        if ($status) {
            if ($stmt->rowCount() > 0) {
                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    $tmp_dimensions = [];
                    foreach ($row as $key => $value) {
                        array_push($tmp_dimensions, $value);                
                    }

                    if (count($tmp_dimensions) > 0) {                        
                        array_push($result, $tmp_dimensions);
                    }
                }
            }
        }
        else {
            echo "<p>Error ".$stmt->errorCode()."\nMessage ".implode($stmt->errorInfo())."</p>\n";
            exit(1);
		}
		
		
		return $result;	

		$dbc = NULL;
  }
?>
